package config

import (
	"fmt"
	"strconv"

	"github.com/spf13/viper"
)

type Config struct {
	port int
}

var appConfig Config

func Load() {
	viper.SetDefault("APP_PORT", "8080")

	viper.SetConfigName("application")
	viper.SetConfigType("yaml")

	viper.AddConfigPath("./")
	viper.AddConfigPath("../")
	viper.AddConfigPath("../../")

	viper.ReadInConfig()
	viper.AutomaticEnv()

	appConfig = Config{
		port: extractIntValue("APP_PORT"),
	}
}

func Port() int {
	return appConfig.port
}

func extractIntValue(key string) int {
	checkPresenceOf(key)
	value, err := strconv.Atoi(viper.GetString(key))
	if err != nil {
		panic(fmt.Sprintf("key %s is not a valid integer value", key))
	}
	return value
}

func checkPresenceOf(key string) {
	if !viper.IsSet(key) {
		panic(fmt.Sprintf("key %s is not set", key))
	}
}
