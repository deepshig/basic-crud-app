package config

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestShouldLoadConfigFile(t *testing.T) {
	Load()

	assert.NotNil(t, Port())
}

func TestShouldLoadConfigFromEnvironmentVariables(t *testing.T) {
	configEnvVars := map[string]string{
		"APP_PORT": "8888",
	}

	for key, val := range configEnvVars {
		err := os.Setenv(key, val)
		assert.NoError(t, err, "Unable to set OS Environment Config Variable for "+key+" key")
	}

	Load()

	assert.Equal(t, 8888, Port())
}
