package main

import (
	"fmt"
	"os"

	"github.com/urfave/cli"
)

func main() {

	app := cli.NewApp()
	app.Name = "basic-crud-app"
	app.Version = "0.0.1"
	app.Action = func(c *cli.Context) error {
		fmt.Println("App is Running")
		return nil
	}

	if err := app.Run(os.Args); err != nil {
		panic(err)
	}

}
