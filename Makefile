.PHONY: all
all: build fmt vet lint test

APP=basic-crud-app
APP_EXECUTABLE="./out/$(APP)"

GLIDE_NOVENDOR=$(shell glide novendor)
UNIT_TEST_PACKAGES=$(shell glide novendor | grep -v "featuretest")

setup:
	curl https://glide.sh/get | sh
	go get -u github.com/golang/lint/golint

build-deps:
	glide install

update-deps:
	glide update

compile:
	mkdir -p out/
	go build -o $(APP_EXECUTABLE)

build: build-deps compile fmt vet lint

install:
	go install $(GLIDE_NOVENDOR)

fmt:
	go fmt $(GLIDE_NOVENDOR)

vet:
	go vet $(GLIDE_NOVENDOR)

lint:
	@for p in $(UNIT_TEST_PACKAGES); do \
		echo "==>Linting $$p"; \
		golint $$p | { grep -vwE "exported (var|function|method|type|const) \S+ should have comment" || true; } \
		done

test:
	ENVIRONMENT=test go test $(UNIT_TEST_PACKAGES) -p=1

copy-config:
	cp application.yml.sample application.yml

